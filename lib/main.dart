// This sample shows adding an action to an [AppBar] that opens a shopping cart.

import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Code Sample for material.Scaffold',
      theme: ThemeData(
        primarySwatch: Colors.lightGreen,
      ),
      home: MyStatefulWidget(),
    );
  }
}

class MyStatefulWidget extends StatefulWidget {
  MyStatefulWidget({Key key}) : super(key: key);

  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  var _isCtoF = true;

  final _ftextEditingController = TextEditingController();
  final _ctextEditingController = TextEditingController();

  @override
  void initState() {
    super.initState();

    _ctextEditingController.addListener(_handleConvertToF);
    _ftextEditingController.addListener(_handleConvertToC);
  }

  @override
  void dispose() {
    // Clean up the controller when the Widget is removed from the Widget tree
    // This also removes the _printLatestValue listener
    _ctextEditingController.dispose();
    _ftextEditingController.dispose();
    super.dispose();
  }

  _handleConvertToF() {
    if (_ctextEditingController.text.trim().length > 0 && _isCtoF) {
      _ftextEditingController.text = calculateCtoF(double.parse(_ctextEditingController.text)).toString();
    }
  }

  _handleConvertToC() {
    if (_ftextEditingController.text.trim().length > 0 && !_isCtoF) {
      _ctextEditingController.text = calculateFtoC(double.parse(_ftextEditingController.text)).toString();
    }
  }

  double calculateCtoF(double num) {
    return (num * 9/5) + 32;
  }

  double calculateFtoC(double num) {
    return (num - 32) * 5/9;
  }

  void handleConvert() {

  }

  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Convert Temperature'),
        ),
        body: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  width: 300,
                  height: 100,
                  child: TextField(
                    decoration: InputDecoration(
                      labelText: "Celsius Degrees",
                    ),
                    onTap:() => setState((){
                      _isCtoF = true;
                    }),
                    controller: _ctextEditingController,
                    textAlign: TextAlign.center,
                    keyboardType: TextInputType.numberWithOptions(signed: false, decimal: false,)
                    ,),
                ),
                Container(
                  width: 300,
                  height: 50,
                  child: TextField(
                    decoration: InputDecoration(
                      labelText: "Fahrenheit Degrees",
                    ),
                    onTap:() => setState((){
                      _isCtoF = false;
                    }),
                    controller: _ftextEditingController,
                    textAlign: TextAlign.center,
                    keyboardType: TextInputType.numberWithOptions(signed: false, decimal: false,)
                    ,),
                )
              ],
            ),
          ],
        ),


      bottomNavigationBar: BottomAppBar(
        child: Container(
          height: 50.0,
        ),
      ),
//      floatingActionButton: FloatingActionButton(
//        onPressed: handleConvert,
//        tooltip: 'Increment Counter',
//        child: Icon(Icons.play_arrow),
//      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }
}

